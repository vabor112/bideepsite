import os, sys, time, random, re
import traceback
import pickle, h5py
import numpy as np

from multiprocessing import Process, Manager, Pool
from contextlib import closing

import keras

from keras.models import Sequential, Model
from keras.layers import Conv3D, MaxPooling3D, Dropout, Flatten, Dense
from keras.layers import Activation, Input, Multiply, Reshape, Lambda
from keras.layers.advanced_activations import ELU
from keras.optimizers import Adam
from keras.utils import Sequence
from keras.callbacks import Callback
from keras import backend as K

from utils import DataGeneratorBalanced, TimingCallback, product

file_with_prefixes_data_name = 'chosen_samples.pkl'
DATA_DIR = 'data'
BATCH_SIZE = 128

def get_compiled_deepsite_model_with_attention(boxsize=16):
	# Some constants
	input_shape = (boxsize, boxsize, boxsize, 8)
	first_conv_out_shape = (boxsize, boxsize, boxsize, 32)
	dense_input_shape = product(input_shape)
	dense_output_shape = 32

	def apply_attention(inp):
		import tensorflow as tf
		x = inp[0]
		attention = inp[1]
		attention = tf.expand_dims(attention, 1)
		attention = tf.expand_dims(attention, 1)
		attention = tf.expand_dims(attention, 1)
		return tf.multiply(x, attention)

	# Attention_layer
	inputs = Input(shape=input_shape)
	first_conv = Conv3D(32, (8, 8, 8), padding='same', activation=K.elu,
			input_shape=input_shape, data_format='channels_last',
			name='conv1')(inputs)
	attention_probs = Flatten()(inputs)
	attention_probs = Dense(dense_output_shape,
			input_shape=(dense_input_shape,),
			activation='softmax', name='attention_probs')(attention_probs)
	attention_mul = Lambda(apply_attention)([first_conv, attention_probs])
	attention_layer = Model(inputs=inputs, outputs=attention_mul)

	# Main sequential model
	model = Sequential()
	model.add(attention_layer)
	model.add(Conv3D(48, (4, 4, 4), name='conv2'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(64, (4, 4, 4), padding='same', name='conv3'))
	model.add(ELU())
	model.add(Conv3D(96, (4, 4, 4), name='conv4'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_compiled_deepsite_model_with_attention_on_inputs(boxsize=16):
	# Some constants
	input_shape = (boxsize, boxsize, boxsize, 8)
	dense_input_shape = product(input_shape)

	def apply_attention(inp):
		import tensorflow as tf
		x = inp[0]
		attention = inp[1]
		attention = tf.expand_dims(attention, 1)
		attention = tf.expand_dims(attention, 1)
		attention = tf.expand_dims(attention, 1)
		return tf.multiply(x, attention)

	# Attention_layer
	inputs = Input(shape=input_shape)
	attention_probs = Flatten()(inputs)
	attention_probs = Dense(8,
			input_shape=(dense_input_shape,),
			activation='softmax', name='attention_probs')(attention_probs)
	attention_mul = Lambda(apply_attention)([inputs, attention_probs])
	attention_layer = Model(inputs=inputs, outputs=attention_mul)

	# Main sequential model
	model = Sequential()
	model.add(attention_layer)
	model.add(Conv3D(32, (8, 8, 8), padding='same', input_shape=(boxsize, boxsize, boxsize, 8),
	data_format='channels_last', name='conv1'))
	model.add(ELU())
	model.add(Conv3D(48, (4, 4, 4), name='conv2'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(64, (4, 4, 4), padding='same', name='conv3'))
	model.add(ELU())
	model.add(Conv3D(96, (4, 4, 4), name='conv4'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_compiled_deepsite_model_with_alt_attention(boxsize=16):
	# Some constants
	input_shape = (boxsize, boxsize, boxsize, 8)
	first_conv_out_shape = (boxsize, boxsize, boxsize, 32)
	dense_output_shape = 32

	def apply_attention(inp):
		import tensorflow as tf
		x = inp[0]
		attention = inp[1]
		attention = tf.expand_dims(attention, 1)
		attention = tf.expand_dims(attention, 1)
		attention = tf.expand_dims(attention, 1)
		return tf.multiply(x, attention)

	# Attention_layer
	inputs = Input(shape=input_shape)
	first_conv = Conv3D(32, (8, 8, 8), padding='same', activation=K.elu,
			input_shape=input_shape, data_format='channels_last',
			name='conv1')(inputs)
	attention_probs = Flatten()(first_conv)
	attention_probs = Dense(dense_output_shape,
			input_shape=(product(first_conv_out_shape),),
			activation='softmax', name='attention_probs')(attention_probs)
	attention_mul = Lambda(apply_attention)([first_conv, attention_probs])
	attention_layer = Model(inputs=inputs, outputs=attention_mul)

	# Main sequential model
	model = Sequential()
	model.add(attention_layer)
	model.add(Conv3D(48, (4, 4, 4), name='conv2'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(64, (4, 4, 4), padding='same', name='conv3'))
	model.add(ELU())
	model.add(Conv3D(96, (4, 4, 4), name='conv4'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_compiled_deepsite_model_with_stupid_attention(boxsize=16):
	# Some constants
	input_shape = (boxsize, boxsize, boxsize, 8)
	first_conv_out_shape = (boxsize, boxsize, boxsize, 32)
	dense_input_shape = product(input_shape)
	dense_output_shape = product(first_conv_out_shape)

	# Attention_layer
	inputs = Input(shape=input_shape)
	first_conv = Conv3D(32, (8, 8, 8), padding='same', activation=K.elu,
			input_shape=input_shape, data_format='channels_last',
			name='conv1')(inputs)
	attention_probs = Flatten()(inputs)
	attention_probs = Dense(dense_output_shape,
			input_shape=(dense_input_shape,),
			activation='softmax')(attention_probs)
	attention_probs = Reshape(first_conv_out_shape)(attention_probs)
	attention_mul = Multiply()([first_conv, attention_probs])
	attention_layer = Model(inputs=inputs, outputs=attention_mul)

	# Main sequential model
	model = Sequential()
	model.add(attention_layer)
	model.add(Conv3D(48, (4, 4, 4), name='conv2'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(64, (4, 4, 4), padding='same', name='conv3'))
	model.add(ELU())
	model.add(Conv3D(96, (4, 4, 4), name='conv4'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_compiled_deepsite_model(boxsize=16):
	model = Sequential()
	model.add(Conv3D(32, (8, 8, 8), padding='same', input_shape=(boxsize, boxsize, boxsize, 8),
	data_format='channels_last', name='conv1'))
	model.add(ELU())
	model.add(Conv3D(48, (4, 4, 4), name='conv2'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(64, (4, 4, 4), padding='same', name='conv3'))
	model.add(ELU())
	model.add(Conv3D(96, (4, 4, 4), name='conv4'))
	model.add(ELU())
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_compiled_exotic_model_1(boxsize=16):
	# exotic model 1
	model = Sequential()
	model.add(Conv3D(64, (3, 3, 3), padding='valid',
		input_shape=(boxsize, boxsize, boxsize, 8),
		data_format='channels_last', name='v2.conv1.1',
		activation = K.elu))
	model.add(Conv3D(64, (3, 3, 3), padding='valid',
		data_format='channels_last', name='v2.conv1.2',
		activation = K.elu))
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(128, (3, 3, 3), padding='valid',
		data_format='channels_last', name='v2.conv2.1',
		activation = K.elu))
	model.add(Conv3D(128, (3, 3, 3), padding='valid',
		data_format='channels_last', name='v2.conv2.2',
		activation = K.elu))
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_compiled_exotic_model_2(boxsize=16):
	# exotic model 2
	model = Sequential()
	model.add(Conv3D(64, (3, 3, 3), padding='same',
		input_shape=(boxsize, boxsize, boxsize, 8),
		data_format='channels_last', name='v3.conv1.1',
		activation = K.elu))
	model.add(Conv3D(64, (3, 3, 3), padding='same',
		data_format='channels_last', name='v3.conv1.2',
		activation = K.elu))
	model.add(Conv3D(64, (3, 3, 3), padding='same',
		data_format='channels_last', name='v3.conv1.3',
		activation = K.elu))
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv3D(128, (3, 3, 3), padding='valid',
		data_format='channels_last', name='v3.conv2.1',
		activation = K.elu))
	model.add(Conv3D(128, (3, 3, 3), padding='valid',
		data_format='channels_last', name='v3.conv2.2',
		activation = K.elu))
	model.add(Conv3D(128, (3, 3, 3), padding='valid',
		data_format='channels_last', name='v3.conv2.3',
		activation = K.elu))
	model.add(MaxPooling3D(pool_size=(2, 2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, name='dense1'))
	model.add(ELU())
	model.add(Dropout(0.5))
	model.add(Dense(1, name='dense2'))
	model.add(Activation('sigmoid'))
	opt = Adam()
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model

def get_best_weights_file_name(out_suffix):
	file_names = os.listdir('weights')
	print(file_names)
	saved_weights_regex = re.compile('weights_%s.([0-9\.]*)-([0-9\.]*).hdf5' % out_suffix)
	max_acc = 0.0
	max_acc_file_name = None
	max_epoch = 0
	for file_name in file_names:
		res = saved_weights_regex.search(file_name)

		if not res:
			continue
		print(file_name)

		cur_epoch = int(res.group(1))
		cur_acc = float(res.group(2))

		if cur_acc >= max_acc and (cur_epoch > max_epoch):
			max_acc = cur_acc
			max_epoch = cur_epoch
			max_acc_file_name = file_name
	
	if max_acc_file_name:
		return max_acc_file_name
	else:
		raise Exception('Something went wrong.')

############################# Constructing model ###############################

model = get_compiled_deepsite_model()

#model = get_compiled_deepsite_model_with_attention()
#load_filename = os.path.join('weights', 'model_basic_train_25_epochs_best.h5') 
#model.load_weights(load_filename, by_name=True)
#loaded_model = keras.models.load_model(load_filename)
#submodel_layer_dict = dict([(layer.name, layer) for layer in model.layers[0].layers])
#loaded_model_layer_dict = dict([(layer.name, layer) for layer in loaded_model.layers])
#submodel_layer_dict['conv1'].set_weights(loaded_model_layer_dict['conv1'].get_weights())

#model = get_compiled_deepsite_model_with_attention_on_inputs()
#load_filename = os.path.join('weights', 'model_basic_train_25_epochs_best.h5') 
#model.load_weights(load_filename, by_name=True)

##model = get_compiled_deepsite_model_with_alt_attention()
##load_filename = os.path.join('weights', 'model_basic_train_25_epochs_best.h5') 
##model.load_weights(load_filename, by_name=True)
##loaded_model = keras.models.load_model(load_filename)
##submodel_layer_dict = dict([(layer.name, layer) for layer in model.layers[0].layers])
##loaded_model_layer_dict = dict([(layer.name, layer) for layer in loaded_model.layers])
##submodel_layer_dict['conv1'].set_weights(loaded_model_layer_dict['conv1'].get_weights())

#model = get_compiled_exotic_model_1()

#model = get_compiled_exotic_model_2()

######################### Loading prefixes from file ###########################

with open(file_with_prefixes_data_name, 'rb') as file_with_prefixes_data:
	data = pickle.load(file_with_prefixes_data)

	train_prefixes = data['train_prefixes']
	validation_prefixes = data['validation_prefixes']
	test_prefixes = data['test_prefixes']

########################### Benchmark of generators ############################

#training_proteins_in_epoch = int(1.0*len(train_prefixes))
#print('Proteins in epoch: %d.' % training_proteins_in_epoch)
#training_generator = DataGeneratorBalanced(train_prefixes, training_proteins_in_epoch, BATCH_SIZE)
#print('Samples: %d+%d.' % (len(training_generator.x_pos), len(training_generator.x_neg)))
#print('len(training_generator) = %d.' % len(training_generator))
#
#num_of_getitems = 242
#start = time.time()
#for i in range(num_of_getitems):
#	training_generator.__getitem__(i)
#end = time.time()
#
#print('Overall time for', num_of_getitems, '__getitem__:', (end - start))
#print('Mean time for __getitem__:', float(end - start)/num_of_getitems)

########################## Training and evaluating model #######################

out_suffix = sys.argv[1]

the_start = time.time()
print('========================= Constructing generators =============================')
training_generator = DataGeneratorBalanced(train_prefixes,
		len(train_prefixes), BATCH_SIZE, DATA_DIR)
validation_generator = DataGeneratorBalanced(validation_prefixes,
		len(validation_prefixes), BATCH_SIZE, DATA_DIR)
test_generator = DataGeneratorBalanced(test_prefixes,
		len(test_prefixes), BATCH_SIZE, DATA_DIR)

if not os.path.exists('weights/'):
	os.makedirs('weights/')
if not os.path.exists('logs/'):
	os.makedirs('logs/')

early_stopping_callback = keras.callbacks.EarlyStopping(monitor='val_acc',
		min_delta=0, patience=25, verbose=1, mode='auto')

model_checkpoint_callback = keras.callbacks.ModelCheckpoint(
	'weights/weights_%s.{epoch:02d}-{val_acc:.2f}.hdf5' % out_suffix, monitor='val_acc',
	verbose=1, save_best_only=True, save_weights_only=False,
	mode='auto', period=1
	)

training_log_file_name = 'logs/training_%s.log' % out_suffix
csv_logger_callback = keras.callbacks.CSVLogger(training_log_file_name)

timing_calback = TimingCallback()

#callbacks = [early_stopping_callback, model_checkpoint_callback, csv_logger_callback]
callbacks = [model_checkpoint_callback, csv_logger_callback, timing_calback]

print('============================= Training model ==================================')
model.fit_generator(generator=training_generator,
		validation_data=validation_generator,
		epochs = 25, verbose=1, callbacks = callbacks)
model.save('weights/model_%s_final.h5' % out_suffix)

print('============================ Evaluating model =================================')
best_weights_name = os.path.join('weights/', get_best_weights_file_name(out_suffix))
print('Loading weights from "%s" as it seems to perform best on validation.' % best_weights_name)
model.load_weights(best_weights_name)
test_loss, test_acc = model.evaluate_generator(training_generator)
print(model.metrics_names[0], ':', test_loss)
print(model.metrics_names[1], ':', test_acc)
model.save('weights/model_%s_best.h5' % out_suffix)

the_end = time.time()
################################### Saving logs ################################

with open(training_log_file_name, 'a') as f:
	f.write(','.join(['test', str(test_acc), str(test_loss), '', '']))

with open('logs/time_log_%s.txt' % out_suffix, 'w') as time_log_f:
	lines = ['Epoch #%d: %d seconds.' % (i, t) for i, t in enumerate(timing_calback.logs)]
	lines.append('Overall: %d seconds.' % (the_end - the_start))
	time_log_f.write('\n'.join(lines))

with open('logs/training_generator_log_%s.txt' % out_suffix, 'w') as f:
	f.write('\n'.join(training_generator.log))
with open('logs/validation_generator_log_%s.txt' % out_suffix, 'w') as f:
	f.write('\n'.join(validation_generator.log))
with open('logs/test_generator_log_%s.txt' % out_suffix, 'w') as f:
	f.write('\n'.join(test_generator.log))
