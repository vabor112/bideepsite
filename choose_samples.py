import random, time, h5py, os, pickle

scPDBdir = 'scPDB' # suppose that all data are located at this folder
data_dir = 'data'
out_file_name = 'chosen_samples.pkl'

uniprot_id_prefixes = dict()
number_of_sites = 0

def check_valid_prefix(prefix):
	X_name = os.path.join(data_dir, prefix + '_X.h5')
	y_name = os.path.join(data_dir, prefix + '_y.h5')
	return (os.path.isfile(X_name) and os.path.isfile(y_name)) 

with open('annotations.tsv') as annotations:
	header = annotations.readline().split('\t')

	uniprot_id_ind = header.index('Uniprot_ID')
	pdb_id_ind = header.index('PDB_ID')
	site_number_ind = header.index('Site_Number')
	while 1:
		cur_line = annotations.readline()

		if not cur_line:
			break

		cur_data = cur_line.split('\t')

		cur_uniprot_id = cur_data[uniprot_id_ind]
		cur_pdb_id = cur_data[pdb_id_ind]
		cur_site_number = cur_data[site_number_ind]

		cur_prefix_name = cur_pdb_id + '_' + cur_site_number

		uniprot_id_prefixes[cur_uniprot_id] = uniprot_id_prefixes.get(cur_uniprot_id, []) + [cur_prefix_name]
		#print(uniprot_id_prefixes[cur_uniprot_id])
		number_of_sites += 1

print('Got %d samples overall.' % number_of_sites)
print('')

unique_uniprot_id_prefixes = [random.choice(prefixes) for _, prefixes in uniprot_id_prefixes.items()]
print('Got %d samples with unique uniprot id.' % len(unique_uniprot_id_prefixes))
print('')
unique_uniprot_id_prefixes = list(filter(check_valid_prefix, unique_uniprot_id_prefixes))
print('Got %d valid samples with unique uniprot id.' % len(unique_uniprot_id_prefixes))
print('')

N = len(unique_uniprot_id_prefixes)
random.shuffle(unique_uniprot_id_prefixes)
train = unique_uniprot_id_prefixes[:int(0.8*N)]
validation = unique_uniprot_id_prefixes[int(0.8*N):int(0.9*N)]
test = unique_uniprot_id_prefixes[int(0.9*N):]

print('Got %d valid samples with unique uniprot id for train.' % len(train))
print('Got %d valid samples with unique uniprot id for validation.' % len(validation))
print('Got %d valid samples with unique uniprot id for test.' % len(test))
print('')

with open(out_file_name, 'wb') as out_file:
	pickle.dump({'train_prefixes': train, 'validation_prefixes': validation, 'test_prefixes': test}, out_file, 2)
	
print('Saved prefixes data to file "%s".' % out_file_name)
