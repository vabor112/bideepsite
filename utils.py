import os, sys, time, random, re, traceback, h5py
import numpy as np

from multiprocessing import Process, Manager, Pool
from contextlib import closing

from keras.utils import Sequence
from keras.callbacks import Callback

def load_prefix(data):
	x_pos, x_neg, cur_X_name, cur_y_name = data
	with h5py.File(cur_X_name, 'r') as cur_X_f, h5py.File(cur_y_name, 'r') as cur_y_f:
		x_pos += [xx for xx, yy in zip(cur_X_f['main'], cur_y_f['main']) if yy]
		x_neg += [xx for xx, yy in zip(cur_X_f['main'], cur_y_f['main']) if not yy]

class DataGeneratorBalanced(Sequence):

	def __init__(self, prefixes, proteins_in_epoch, batch_size, DATA_DIR,
			NUMBER_OF_PROCESSES = 50, X_SUFFIX = '_X.h5', y_SUFFIX = '_y.h5'):
		assert(batch_size % 2 == 0)
		assert(proteins_in_epoch <= len(prefixes))

		self.proteins_in_epoch = proteins_in_epoch
		self.batch_size = batch_size
		self.prefixes = prefixes
		self.DATA_DIR = DATA_DIR
		self.NUMBER_OF_PROCESSES = NUMBER_OF_PROCESSES
		self.X_SUFFIX = X_SUFFIX
		self.y_SUFFIX = y_SUFFIX

		self.cur_epoch_counter = 0
		self.x_pos, self.x_neg = None, None
		self.log = []

		self.load_data()
		self.on_epoch_end()

	def __len__(self):
		return int(2.0*len(self.x_pos) / self.batch_size)

	def load_data(self):
		self.log.append('')
		self.log.append('Starting "load_data".')
		start_overall = time.time()
		self.x_pos = []
		self.x_neg = []

		if (self.cur_epoch_counter + 1)*self.proteins_in_epoch > len(self.prefixes):
			self.prefixes = random.shuffle(self.prefixes)
			self.cur_epoch_counter = 0

		cur_prefix_start_ind = self.cur_epoch_counter*self.proteins_in_epoch
		cur_prefix_finish_ind = cur_prefix_start_ind + self.proteins_in_epoch
		self.log.append('\tPrefixes interval: [%d, %d).' % (cur_prefix_start_ind, cur_prefix_finish_ind))

		def construct_arguments_list(x_pos, x_neg):
			result = []
			for i in range(cur_prefix_start_ind, cur_prefix_finish_ind):
				cur_X_name = os.path.join(self.DATA_DIR, self.prefixes[i] + self.X_SUFFIX)
				cur_y_name = os.path.join(self.DATA_DIR, self.prefixes[i] + self.y_SUFFIX)
				result.append((x_pos, x_neg, cur_X_name, cur_y_name))
			return result

		start = time.time()
		if self.NUMBER_OF_PROCESSES > 1:
			self.log.append('\tLoading data using %d subprocess.' % self.NUMBER_OF_PROCESSES)
			with Manager() as manager:

				x_pos = manager.list()
				x_neg = manager.list()

				arguments_for_processes = construct_arguments_list(x_pos, x_neg)

				with closing(Pool(processes=self.NUMBER_OF_PROCESSES)) as p:
					p.map(load_prefix, arguments_for_processes)
					p.terminate()

				self.x_pos = list(x_pos)
				self.x_neg = list(x_neg)
		else:
			self.log.append('\tLoading data without multiprocessing.')

			x_pos = []
			x_neg = []

			arguments_for_processes = construct_arguments_list(x_pos, x_neg)

			for data in arguments_for_processes:
				load_prefix(data)

			self.x_pos = x_pos
			self.x_neg = x_neg

		end = time.time()
		self.log.append('\t\tIt took %d seconds.' % (end - start))

		end_overall = time.time()
		self.log.append('\t"load_data" finished in %d seconds.' % (end_overall - start_overall))
	
	def on_epoch_end(self):
		self.log.append('')
		self.log.append('Starting "on_epoch_end".')
		start_overall = time.time()

		if self.proteins_in_epoch < len(self.prefixes):
			self.log.append('\tLaunching "load_data".')
			self.load_data()

		self.log.append('\tApplying permutation.')
		start = time.time()

		random.shuffle(self.x_pos)
		random.shuffle(self.x_neg)
		end = time.time()
		self.log.append('\t\tIt took %d seconds.' % (end - start))
		self.log.append('')

		self.cur_batch_counter = 0
		end_overall = time.time()
		self.log.append('\t"on_epoch_end" finished in %d seconds.' % (end_overall - start_overall))

	def __getitem__(self, idx):
		self.log.append('Starting "__getitem__".')
		start = time.time()
		if (self.cur_batch_counter >= self.__len__()):
			self.log.append('\tForce-launching on_epoch_end.')
			self.on_epoch_end()
		cur_start_ind = self.cur_batch_counter*self.batch_size/2
		cur_finish_ind = cur_start_ind + self.batch_size/2
		self.cur_batch_counter += 1


		permutation = list(range(self.batch_size))
		random.shuffle(permutation)

		x, y = [], []
		for i in range(self.batch_size):
			if permutation[i] < self.batch_size/2:
				cur_ind = cur_start_ind + permutation[i]
				x.append(self.x_pos[cur_ind])
				y.append(1)
			else:
				cur_ind = cur_start_ind + permutation[i] - self.batch_size/2
				x.append(self.x_neg[cur_ind])
				y.append(0)
		result = np.array(x), np.array(y)
		end = time.time()
		self.log.append('\t"__getitem__" finished in %d seconds.' % (end - start))
		return result

class TimingCallback(Callback):
	def __init__(self):
		self.logs=[]
	def on_epoch_begin(self, epoch, logs={}):
		self.starttime=time.time()
	def on_epoch_end(self, epoch, logs={}):
		self.logs.append(time.time()-self.starttime)

def product(l):
	return reduce(lambda x, y: x * y, l, 1)
