# BIDeepSite

## Preliminaries
In 2017 [the paper](https://www.ncbi.nlm.nih.gov/pubmed/28575181) came out proposing to use the deep convolutional neural network (DCNN), which they called DeepSite, to solve the problem of the protein-ligand binding site (binding pocket) detection.

More precisely, they proposed to represent a protein as a 3D-image with 8 channels (see image below) discretizing the protein 3D-structure into a grid of 1x1x1 ų sized voxels and computing channel values for each voxel based on the properties of the neighbouring atoms.

![Channels](img/channels.png)

The neural network they proposed takes 16x16x16 slice of the 8-channel 3D-image and tries to predict if the center of this slice is closer than a given threshold to the center of any binding site. See image below for the architecture they proposed.
![From the paper](img/architecture.png)

The data for the training was taken from the [scPDB](http://bioinfo-pharma.u-strasbg.fr/scPDB/) database (the authors used the version of 2013) which contains ~9000 pairs (protein, ligand) with annotated binding site. It was filtered and balanced (of course, in the real data the number of 16x16x16 slices which are far from all binding pockets will be much greater than the number of those which are close to any, hence balancing is needed).

The authors report the 98.4% accuracy of the trained DeepSite DCNN.

**Note:** Though the 98.4% accuracy of DCNN may look very nice, it does not really guarantee a good quality of the identified binding sites (see [the paper](https://www.ncbi.nlm.nih.gov/pubmed/28575181), where authors, after training DCNN, evaluate its results using more appropriate metrics and compare the results with other binding site detectors). For example, authors report that the DVO metric, which computes the Jaccard Index of the pair (convex hull of a predicted binding site, convex hull of a real binding site), is 0.652 on the average. Which, while being state-of-the-art performance, doesn't look as good as the DCNN's 98.4% accuracy. See some sample prediction in the ["Sample prediction" section](#markdown-header-sample-prediction).

## What did **we** do

*  We reproduced the pipeline from [the paper](https://www.ncbi.nlm.nih.gov/pubmed/28575181) from preprocessing to DCNN. We used the most recent version of [scPDB](http://bioinfo-pharma.u-strasbg.fr/scPDB/) and confirmed the 98.4% accuracy of the proposed DCNN architecture. Published the code in this repo (details on running this code are described in the ["Pipeline" section](#markdown-header-pipeline)).
*  Proposed a modified DCNN architecture with 99.9% accuracy (see picture below). It is included in the pipeline as an option.
![Improved accuracy](img/exotic_architecture.png)
*  Used some special attention layer on inputs (see architecture on picture below) added to the pre-trained model to assign some importances to each channel. 
![Attention on inputs](img/attention_on_inputs_architecture.png)
By a wide margin, ``Occupancy`` channel (which is responsible for geometry) is the most important one. Then follows ``Hydrophobic`` channel, and then everything else. See mean activations of the corresponding attention parameters on the picture below.
![Mean activations](img/attention_on_inputs.png)
*  Published pre-trained models (see ["Saved models" section](#markdown-header-saved-models) for details).

## Pipline

### Techincal notes
Note the following.

1. We will denote the path of the base folder of this repo by ``..../``.
2. It's highly recommended that you run the pipeline on a powerful computer (something with a few dozens of CPU cores and a GPU with CUDA support).

### I. Preprocessing of scPDB data
At the beginning of this step you will need the following.

*  Python 3 (most probably inside Conda environment, see further).
*  The data from [scPDB](http://bioinfo-pharma.u-strasbg.fr/scPDB/) downloaded and unpacked to the ``..../scPDB/`` folder.
*  [HTMD (High-Throughput Molecular Dynamics)](https://software.acellera.com/academic-download.html) Python library. It runs well only in Conda environment, so all the other required Python libraries (which are listed in the following item) need to be installed using Conda as well.
*  ``pickle``, ``h5py``, ``scipy`` Python libraries.

Run the script ``feature_extractor.py`` (requires no arguments) from the repo base folder. In the end you will have new ``..../data/`` folder with processed data (processing includes balancing).

**Note:** This may take some time. For example, it takes about 6 hours when using 25 CPU cores.

**Note:** The ``..../data/`` folder will have size of about 7Gb, be prepared for this.

**Note:** This script creates a pool of 25 processes. If your machine doesn't have enough computing cores to take advantage of so many processes, you probably need to change ``NUMBER_OF_PROCESSES`` global variable to something more appropriate.

**Note:** Besides processed data, there will be a file ``..../data/statistics.pkl`` containing some statistics about data and some benchmarking information.

### II. Filtering and separating data into train/validation/test sets
At the beginning of this step you will need the following.

*  Python 3.
*  Processed scPDB data obtained from the previous step.
*  ``pickle``, ``h5py`` Python libraries.

Run the script ``choose_samples.py`` (requires no arguments) from the repo base folder. It will use the data from ``..../data/`` folder and from ``..../annotations.tsv`` file to choose unique (protein, ligand) pair for each Uniprot_ID present in the data. It will also separate data into train, validation and test sets (80%, 10%, 10%). In the end it will produce the ``chosen_samples.pkl`` file which will be needed for the neural network training.

**Note:** This scripts runs pretty fast and does not require much computational resources.

**Note:** Actually, it will be better if you would replace the repo's ``..../annotations.tsv`` with the most recent version. To obtain the most recent version, run an empty search at [scPDB site](http://bioinfo-pharma.u-strasbg.fr/scPDB/FORM) and then export data to csv.

### III. Training neural network

At the beginning of this step you will need the following.

*  Python **2**. Yes, sorry for that, but this part of the pipeline should use Python **2**, we had our reasons for that.
*  ``..../data/`` folder generated on the [step I](#markdown-header-i-preprocessing-of-scpdb-data) and the ``chosen_samples.pkl`` file from the [step II](#markdown-header-ii-filtering-and-separating-data-into-trainvalidationtest-sets).
*  ``tensorflow`` or, much better, ``tensorflow-gpu`` python library.
*  ``numpy``, ``pickle``, ``h5py`` Python 2 libraries.

To train the architecture from the paper just run ``deep_site.py`` script like this
```bash
deep_site.py YOUR_SUFFIX
```
where ``YOUR_SUFFIX`` is the suffix that will be added to all the files (weights, logs) that the script produces. This will produce several log files which will be put into ``..../logs`` directory and save several model versions to the ``..../weights`` directory. The model with best accuracy on validation will be in the file ``weights/model_YOUR_SUFFIX_best.h5``.

**Note:** This is very computational expensive.

**Note:** To choose non-standard architecture comment the ``model = get_compiled_deepsite_model()`` line and uncomment lines that correspond to the architecture you are interested in.

## Saved models
Several interesting trained models can be found in the ``..../chosen_weights/`` folder.

*  ``model_basic_train_25_epochs_best.h5`` --- model from the paper (accuracy on test ~98.4%).
![From the paper](img/vanilla_architecture.png)
*  ``model_exotic_50_epochs_best.h5`` --- improved architecture (accuracy on test ~99.9%).
![Improved accuracy](img/exotic_architecture.png)
*  ``model_attention_on_inputs_25_epochs_best.h5`` --- model with attention on inputs which we used to assign importances to each channel in inputs.
![Attention on inputs](img/attention_on_inputs_architecture.png)
*  ``model_attention_on_conv1_25_epochs_best.h5`` --- model with attention on conv1 (one weight for one convolution filter) which can be used to assign importances to the filters.
![Attention on conv1](img/attention_on_conv1_architecture.png)

## Sample prediction
Here is an example of the prediction made using the improved architecture of DCNN.

![Sample prediction](img/binding_site_sample.png)
Points that DCNN classified as close to any binding pocket (threshold of 4� has been used) are highlighted in *green*. The big *grey* thing is a protein and the *blue* little things are some ligands.

