import os, sys, time, random, re
import traceback
import pickle, h5py
import numpy as np

from multiprocessing import Process, Manager, Pool
from contextlib import closing

import keras
import tensorflow as tf

from keras.models import Sequential, Model
from keras.layers import Conv3D, MaxPooling3D, Dropout, Flatten, Dense
from keras.layers import Activation, Input, Multiply, Reshape, Lambda
from keras.layers.advanced_activations import ELU
from keras.optimizers import Adam
from keras.utils import Sequence
from keras.callbacks import Callback
from keras import backend as K

sys.path.insert(1, os.path.join(sys.path[0], '..'))
from utils import DataGeneratorBalanced, TimingCallback, product
from read_activations import get_activations, display_activations

file_with_prefixes_data_name = '../chosen_samples.pkl'
DATA_DIR = '../data'
BATCH_SIZE = 128

with open(file_with_prefixes_data_name, 'rb') as file_with_prefixes_data:
	data = pickle.load(file_with_prefixes_data)

	train_prefixes = data['train_prefixes']
	validation_prefixes = data['validation_prefixes']
	test_prefixes = data['test_prefixes']

model_file_name = sys.argv[1]
model = keras.models.load_model(model_file_name)

print('========================= Constructing generators =============================')
the_generator = DataGeneratorBalanced(test_prefixes,
		len(test_prefixes), BATCH_SIZE, DATA_DIR)
data = the_generator.x_pos + the_generator.x_neg
print(model.layers)
print(model.layers[0].layers)
activations = get_activations(model.layers[0], data, print_shape_only=True)
activations = activations[2] # for attention on inputs
#activations = activations[3] # for attention on conv1
activations = np.array(activations)
activations_transposed = np.transpose(activations)

for i, act in enumerate(activations_transposed):
	print('Activation #%d: mean = %0.5f, std = %0.5f.' % (i+1, np.mean(act), np.std(act)))
