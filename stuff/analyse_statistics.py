from random import shuffle
from math import ceil
import time
import pickle
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from statistics import mean, stdev

file_name = sys.argv[1]
out_prefix = sys.argv[2]

with open(file_name, 'rb') as the_file:
	data = pickle.load(the_file)


num_of_samples_in_dirs = data['num_of_samples_in_dirs']
times_for_dirs = data['times_for_dirs']
num_of_dirs = data['num_of_dirs']


exception_rate = 1.0 - len(num_of_samples_in_dirs)/num_of_dirs
print('Exception rate: %0.2f.' % exception_rate, 'Exceptions occured in %d of %d cases.' % (num_of_dirs - len(num_of_samples_in_dirs), num_of_dirs))

plt.clf()
plt.hist(num_of_samples_in_dirs)
figure = plt.gcf()
plt.savefig(out_prefix + '_num_of_samples_hist.png', dpi=100)    
print('Mean number of samples in folder: %0.2f' % mean(num_of_samples_in_dirs))

plt.clf()
plt.hist(times_for_dirs)
figure = plt.gcf()
plt.savefig(out_prefix + '_times_for_dirs_hist.png', dpi=100)    
print('Mean time for folder: %0.2f' % mean(times_for_dirs))
