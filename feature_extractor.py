from multiprocessing import Pool, Manager
from random import shuffle
from math import ceil
import time
import pickle
import h5py

from htmd.ui import *
from htmd.molecule.voxeldescriptors import getVoxelDescriptors
from scipy.spatial.distance import euclidean


NUMBER_OF_PROCESSES = 25

def get_windows_data(data, ndims=(16, 16, 16)):
	"""helper iterator method: gets data, slices it to windows defined by ndims, returns its coordinates 
	"""
	if isinstance(ndims, tuple):
		(nx, ny, nz) = ndims
	else:
		nx = ny = nz = ndims
	(maxx, maxy, maxz) = data.shape[:3]
	# the following might be ineffective, but simple
	for i in range(0, maxx - nx, 4):
		for j in range(0, maxy - ny, 4):
			for k in range(0, maxz - nz, 4):
				yield i, j, k, data[i:i+nx, j:j+ny, k:k+nz, :]

		
def process_folder_balanced(protein_path, ndims=16, cutoff=4):
	"""we return subset with positive and neg values, balanced by downsampling
	"""
	binding_site = Molecule(os.path.join(protein_path, "site.mol2")) 
	binding_site_center = np.mean(binding_site.get('coords'), axis=0)
	protein_molecule = Molecule(os.path.join(protein_path, "protein.mol2"))
	features, centers, N = getVoxelDescriptors(protein_molecule, buffer=8)
	if np.any(N < ndims):
		print("protein is smaller, skip")
		return
	N = tuple(N) + (-1,)
	features = features.reshape(*N)
	centers = centers.reshape(*N)
	coords = [] # coords of the starting point for (16, 16, 16) block
	y = []
	for i, j, k, window_data in get_windows_data(centers, ndims=ndims):
		voxel_center = np.mean(window_data.reshape((-1, window_data.shape[-1])), axis=0)
		coords.append((i, j, k))		
		y.append(euclidean(voxel_center, binding_site_center) < cutoff)
	coords = np.asarray(coords)
	y = np.asarray(y)
	pos_values = np.where(y)[0]
	neg_values = np.where(y==0)[0]
	neg_downsampled = np.random.choice(neg_values, pos_values.shape[0])
	coords = np.concatenate([coords[pos_values], coords[neg_downsampled]])
	y = np.concatenate([y[pos_values], y[neg_downsampled]])
	# next we permute subset
	ids = np.random.permutation(y.shape[0])
	for index in ids:
		(i, j, k) = coords[index]
		yield features[i: i+ndims, j: j+ndims, k: k+ndims, :], y[index]

	
def save_folder_data(protein_folder_name):
	global num_of_samples_in_dirs, times_for_dirs

	start = time.time()

	out_prefix = os.path.join(out_dir, protein_folder_name)

	samples_file_name = out_prefix + '_X.h5'
	labels_file_name = out_prefix + '_y.h5'

	if os.path.isfile(samples_file_name) and os.path.isfile(labels_file_name):
		print('Skipping. Files "%s" and "%s" already exist.' % (samples_file_name, labels_file_name))
		return

	protein_folder_path = os.path.join(scPDBdir, protein_folder_name)

	samples = []
	labels = []
	try:
		for features, y in process_folder_balanced(protein_folder_path, cutoff=4):
			samples.append(features)
			labels.append(y)
		samples = np.array(samples)
		labels = np.array(labels)
		samples_file = h5py.File(samples_file_name, 'w')
		labels_file = h5py.File(labels_file_name, 'w')
		#samples_file.create_dataset('main', data=samples, compression="gzip", compression_opts=5)
		samples_file.create_dataset('main', data=samples, compression="gzip", compression_opts=1)
		labels_file.create_dataset('main', data=labels)
		samples_file.close()
		labels_file.close()
		num_of_samples_in_dirs.append(len(samples))
		end = time.time()
		times_for_dirs.append(end - start)
		print('Saved "%s" and "%s".' % (samples_file_name, labels_file_name))
	except Exception as e:
		print('Exception occured while processing "%s". Error message: "%s".' % (protein_folder_path, str(e)))
	
scPDBdir = "scPDB" # suppose that all data are located at this folder
folder_names = os.listdir(scPDBdir)
out_dir = 'data'

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

manager = Manager()
num_of_samples_in_dirs = manager.list()
times_for_dirs = manager.list()
with Pool(NUMBER_OF_PROCESSES) as p:
	p.map(save_folder_data, folder_names)

out_file_name = os.path.join(out_dir, 'statistics.pkl')
with open(out_file_name, 'wb') as out_file:
	pickle.dump({
		'num_of_samples_in_dirs': list(num_of_samples_in_dirs),
		'times_for_dirs': list(times_for_dirs),
		'num_of_dirs': len(folder_names),
		}, out_file, 2)
